Introducere 5
Capitolul 1. Tehnologii de bază 10
1.1. Medii de comunicație 10
1.2. Clase de adrese, operații cu clase de adrese 11
1.3. Protocoale de nivel 3 12
1.3.1. Protocoale Distance Vector 13
1.3.2. Protocoale Link State 15
1.4. Protocoale folosite pe echipamente de nivel 2 16
1.5. Rețele Wireless 18
Capitolul 2. Software Defined Networking 20
2.1. Istoric 20
2.2. OnePK 24
2.3. Agenți în rețele definite software 29
2.4. OpenStack 32
2.5. Arhitectura SDN 34
2.6. Arhitectura OnePK 35
2.7. Cisco ACI 37
2.8. OpenDaylight 39
Capitolul 3. Cloud Computing 40
3.1. Definirea conceptelor 40
3.2. Internet of Things 42
3.3. Internet of Everything 42
3.4. JSON & REST vs XML & SOAP 44
Capitolul 4. Aplicația BitPK 46
4.1. Baza de Date 46
4.2. Controllerul onePK 47
4.3. Serverul front-end 48
4.4. Serverul back-end 50
Concluzii 54
Bibliografie 55
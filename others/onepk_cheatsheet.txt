InterfaceStateEventType
Enum indicating the type of Interface State Event.
ONEP_IF_STATE_EVENT_LINK: Link state event type
ONEP_IF_STATE_EVENT_LINEPROTO: Line proto state event type
ONEP_IF_STATE_EVENT_ANY: Any state event


Represents the State of the interface.
ONEP_IF_STATE_ADMIN_DOWN: Interface is administratively down (1)
ONEP_IF_STATE_OPER_DOWN: Interface is administratively up, but it is operationally down (2)
ONEP_IF_STATE_OPER_UP: Interface is operationally up (3)

----------------------------------
Cand inchizi o interfata (shut):
Interface State:1
Line Proto: 2
Link: 1
State Event Type: 0

%LINK-5-CHANGED: Interface GigabitEthernet0/1, changed state to administratively down
%LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet0/1, changed state to down

------------------------------------
Cand deschizi o interfata (no shut):
Interface State:2 (administratively up but op down)
Line Proto:2
Link:2
State Event Type: 0

Interface State:3 (operationally up)
Line Proto:3
Link:3
State Event Type:0

%LINK-3-UPDOWN: Interface GigabitEthernet0/1, changed state to up
%LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet0/1, changed state to up


db.routers.update({"_id" : ObjectId("56a1e8bf90f0c2caaef2b1fc")},{"status" : "off","statistics" : "Good","description": "Outside East","ip" : "192.168.20.11","events" : "off","name" : "vIOS1","interface":"GigabitEthernet0/0"})
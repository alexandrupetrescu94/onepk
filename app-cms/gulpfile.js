var gulp = require('gulp');
var mainBowerFiles = require('main-bower-files'); //moves main bower files to dest
//var inject = require('gulp-inject'); //inserts script srcs to dest file
//var uglify = require('gulp-uglify'); //minify
var concat = require('gulp-concat'); //bundles files together
var fs = require('fs');
var livereload = require('gulp-livereload');
var nodemon = require('gulp-nodemon');
var jshint = require('gulp-jshint');


gulp.task('default', ['bower'], function () {
	console.log('You are on CMS nothing here yet');
});

gulp.task('bower', function () {
    return gulp.src(mainBowerFiles())
        .pipe(gulp.dest("./public/javascripts/lib"));
});

gulp.task('concat', ['concatExists'], function () {
    return gulp.src('app/**/*.js')
    .pipe(concat('app_concat.js'))
    .pipe(gulp.dest('./app/'));
});

gulp.task('concatExists',function(){
	try {
	    stats = fs.lstatSync('./app/app_concat.js');
		if (stats.isFile())
	        fs.unlinkSync('./app/app_concat.js');
	} catch(e){ 
		console.log("File doesn't exists. Creating new one.")
	}
});

//testing nodemon and livereload
gulp.task('lint', function () {
  gulp.src('./**/*.js')
    .pipe(jshint())
});

gulp.task('backend-dev', function () {
  nodemon({ script: './app.js',
            ext: 'html js',
            tasks: ['lint'] 
        }).on('restart', function () {
            console.log('restarted!')
        })
});

//testing livereload
gulp.task('frontend-dev', function() {
    // listen for changes
    livereload.listen({ start: true });
})

//testing nodemon and livereload
gulp.task('start-dev', ['backend-dev', 'frontend-dev'], function(){

})
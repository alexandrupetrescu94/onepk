angular.module("socketIO", []).provider("$socketIO", function(){
    var host;
    this.setHost = function(h){
        host = h;
    };

    this.$get = ['$window', '$q', function($window, $q){
        var deferred = $q.defer();    
        var conn = $window.io.connect(host);
        deferred.resolve(conn);
        return deferred.promise;
    }];
}); 
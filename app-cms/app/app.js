﻿/// <reference path="_references.js" />
'use strict';

var app = angular.module('app', ['nvd3','ngLodash', 'ngResource', 'ngRoute', 'ngSanitize', 'serverModule', 'colorpicker.module', 'ui.tinymce', 'socketIO'])
    .config(['$routeProvider', '$sceDelegateProvider', 'serverConfig', '$compileProvider', '$socketIOProvider', function ($routeProvider, $sceDelegateProvider, serverConfig, $compileProvider, $socketIOProvider) {
        $routeProvider
        .when('/', {
            templateUrl: 'app/views/routers.html',
            controller: 'routerController'
        })
        .when('/routers', {
            templateUrl: 'app/views/routers.html',
            controller: 'routerController'
        })
        .when('/routers/:id', {
            templateUrl: 'app/views/router.html',
            controller: 'routerController'
        })
        .when('/blacklist', {
            templateUrl: 'app/views/blacklist.html',
            controller: 'blacklistController'
        })
        .when('/configurations', {
            templateUrl: 'app/views/configurations.html',
            controller: 'configurationsController'
        })
        .when('/404', {
            templateUrl: 'app/views/404.html'
        })
        .otherwise({
            redirectTo: '/404'
        });

        $socketIOProvider.setHost("http://localhost:3000");
        $sceDelegateProvider.resourceUrlWhitelist(['self', serverConfig.APIUrl + '**']);
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|file|tel|skype|mailto|im|lync|sip):/);
    }])
    .run();

$(document).ready(function () {
    angular.bootstrap(document, ['app']);
});
﻿app.directive('ccToolbar', function (appConfig) {
    function link($scope, $element, $attrs) {
        $scope.toggleMenu = function () {
            $scope.$emit('toggleMenu', {});
        };
    };
    
    return {
        restrict: 'E',
        transclude: true,
        templateUrl: 'app/directives/toolbar.html',
        link: link
    };
});
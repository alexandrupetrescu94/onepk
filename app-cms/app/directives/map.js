﻿app.directive('ccMap', function () {
    
    var MAP = null;
    var GEOCODER = null;
    var selectedMarker = null;
    var markers = [];
    var allowFocus = false;
    
    function link($scope, $element, $attrs) {
        initMap($scope, $element);
        
        var stopWatch = $scope.$watch('location', function () {
            if ($scope.location.address && $scope.location.geo) {
                $scope.location.lat = $scope.location.geo[1];
                $scope.location.lon = $scope.location.geo[0];
                // tugurlanieeee //
                if (selectedMarker) { stopWatch(); return; }
                ///////////////////
                
                var marker = new google.maps.Marker({
                    map: MAP,
                    address: $scope.location.address,
                    //title: $scope.location.name,
                    position: new google.maps.LatLng($scope.location.geo[1], $scope.location.geo[0]),
                    draggable: true,
                    //icon: 'custom-pin.png'
                });
                addMarker($scope, marker);
                
                selectedMarker = marker;
                allowFocus = true;
                focusMarker(marker);
            }
        }, true);
    }    ;
    
    function initMap($scope, $element) {
        
        var mapDiv = $($element).find("div[name='map-div']")[0];
        MAP = new google.maps.Map(mapDiv, { mapTypeId: google.maps.MapTypeId.ROADMAP });
        GEOCODER = new google.maps.Geocoder();
        markers = [];
        selectedMarker = null;
        
        var topLeft = $scope.defaultTopLeft || { lat: 48, lng: 25 };
        var bottomRight = $scope.defaultBottomRight || { lat: 44, lng: 30 };
        var defaultBounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(topLeft.lat, topLeft.lng),
            new google.maps.LatLng(bottomRight.lat, bottomRight.lng)
        );
        MAP.fitBounds(defaultBounds);
        
        // create searchBox
        var mapSearch = $($element).find("input[name='map-search']")[0];
        MAP.controls[google.maps.ControlPosition.TOP_LEFT].push(mapSearch);
        var searchBox = new google.maps.places.SearchBox(mapSearch);
        
        google.maps.event.addListener(searchBox, 'places_changed', function () {
            var places = searchBox.getPlaces();
            if (places.length == 0) return;
            clearMarkers();
            allowFocus = true;
            
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0, place; place = places[i]; i++) {
                var marker = new google.maps.Marker({
                    map: MAP,
                    address: place.formatted_address,
                    title: place.name,
                    position: place.geometry.location,
                    draggable: true,
                    //icon: 'custom-pin.png'
                });
                bounds.extend(marker.position);
                addMarker($scope, marker);
                
                if (places.length == 1) {
                    selectMarker($scope, marker);
                    return;
                }
            }
            MAP.fitBounds(bounds);
        });
        
        google.maps.event.addListener(MAP, 'click', function (e) {
            clearMarkers();
            
            var marker = new google.maps.Marker({
                map: MAP,
                position: e.latLng,
                draggable: true,
                //icon: 'custom-pin.png'
            });
            
            allowFocus = false;
            addMarker($scope, marker);
            google.maps.event.trigger(marker, 'dragend');
        });
    }    ;
    
    function selectMarker($scope, marker) {
        if (selectedMarker != marker) {
            selectedMarker = marker;
            focusMarker(marker);
        }
        
        $scope.$apply(function () {
            $scope.location.address = marker.address;
            //$scope.location.name = marker.title;
            $scope.location.lat = marker.getPosition().lat();
            $scope.location.lon = marker.getPosition().lng();
        });
    }
    
    function addMarker($scope, marker) {
        markers.push(marker);
        
        google.maps.event.addListener(marker, 'mouseup', function (event) {
            selectMarker($scope, marker);
        });
        
        google.maps.event.addListener(marker, 'dragend', function (event) {
            GEOCODER.geocode({ latLng: marker.position }, function (responses) {
                marker.address = responses && responses.length > 0 ? responses[0].formatted_address : "";
                marker.title = "";
                selectMarker($scope, marker);
            });
        });
    }    ;
    
    function focusMarker(marker) {
        if (!allowFocus) return;
        
        var bounds = new google.maps.LatLngBounds();
        bounds.extend(marker.position);
        MAP.fitBounds(bounds);
        
        var listener = google.maps.event.addListener(MAP, "idle", function () {
            MAP.setZoom(16);
            google.maps.event.removeListener(listener);
        });
    }
    
    function clearMarkers() {
        for (var i = 0, marker; marker = markers[i]; i++)
            marker.setMap(null);
        markers = [];
    }
    
    return {
        restrict: 'E',
        templateUrl: 'app/directives/map.html',
        scope: { location: '=', defaultTopLeft: '=', defaultBottomRight: '=' },
        link: link
    };
});

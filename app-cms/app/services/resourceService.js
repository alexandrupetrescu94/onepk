﻿app.factory('resourceService', function ($timeout, $http, $q, $resource, $location, appConfig) {
    if (!appConfig.apiUrl)
        throw 'apiUrl not specified';
    if (!appConfig.siteUrl)
        throw 'siteUrl not specified';
    
    var workingQueTimeout = 300;
    var workingPromise;
    var workingStartDelay = 400;
    var workingStartPromise;
    
    var token = function () {
        var _t = localStorage.getItem('token');
        if (_t != "undefined" && _t != null) {
            //token is in localstorage
            return JSON.parse(localStorage.getItem('token'));
        } else {
            //obtain token
            $.ajax({
                url: appConfig.siteUrl + '/token',
                method: 'GET',
                async: false,
                success: function (data, textStatus, xhr) {
                    localStorage.setItem('token', JSON.stringify(data));
                    _t = data;
                },
                error: function (xhr, textStatus, err) {
                    if (xhr.status == 401)
                        return window.location.href = appConfig.siteUrl + '/account/login';
                }
            });
            return _t;
        }
    };
    
    var requests = 0;
    var actionTimeouts = [];
    
    var cancelAllRequests = function () {
        for (var i = 0; i < actionTimeouts.length; i++) {
            actionTimeouts[i].resolve();
        }
        actionTimeouts = [];
    };
    
    var decrementRequest = function () {
        if (workingStartPromise)
            $timeout.cancel(workingStartPromise);
        if (requests > 0)
            requests--;
        
        if (requests <= 0) {
            workingPromise = $timeout(function () {
                service.isWorking = false;
            }, workingQueTimeout);
            requests = 0;
        }
    };
    
    var createResource = function (url, paramDefaults, actions, options, isBackground) {
        //most of our requests look like this
        var defaultActions = {
            get: { method: 'GET' },
            save: { method: 'POST' },
            query: { method: 'GET', isArray: true },
            remove: { method: 'DELETE' },
            delete: { method: 'DELETE' },
            update: { method: 'PUT' }
        };
        //merge them with the one specified in the <entity> api service
        var _actions = angular.extend({}, defaultActions, actions);
        /*add a defer for each action timeout
        in order to cancel a request , the defer has to be resolved*/
        Object.keys(_actions).forEach(function (i) {
            var timeoutDefer = $q.defer();
            _actions[i].timeout = timeoutDefer.promise;
            actionTimeouts.push(timeoutDefer);
            if (_actions[i].url)
                _actions[i].url = appConfig.apiUrl + _actions[i].url;
        });
        var resource = $resource(appConfig.apiUrl + url, paramDefaults, _actions, options);
        
        //override each action in the resource so we can attach our code
        Object.keys(_actions).forEach(function (action) {
            //clone the original
            var method = resource[action];
            //rewrite the original, use of closure function to send the resource and action
            resource[action] = (function (res, action) {
                return function (a1, a2, a3, a4) {
                    //find out which parameter is witch
                    var params = {}, data, success, error;
                    switch (arguments.length) {
                        case 4:
                            error = a4;
                            success = a3;
                            //fallthrough
                        case 3:
                        case 2:
                            if (typeof a2 === 'function') {
                                if (typeof a1 === 'function') {
                                    success = a1;
                                    error = a2;
                                    break;
                                }
                                
                                success = a2;
                                error = a3;
                                //fallthrough
                            } else {
                                params = a1;
                                data = a2;
                                success = a3;
                                break;
                            }
                        case 1:
                            if (typeof a1 === 'function') success = a1;
                            else if (/^(POST|PUT|PATCH)$/i.test(action)) data = a1;
                            else params = a1;
                            break;
                        case 0: break;
                        default:
                            throw 'expected up to 4 arguments [params, data, success, error]';
                    }
                    if (!isBackground) {
                        if (workingPromise)
                            $timeout.cancel(workingPromise);
                        
                        workingStartPromise = $timeout(function () {
                            service.isWorking = true;
                            requests++;
                        }, workingStartDelay)
                    }
                    //invoke the clone with the same params
                    params.access_token = token().access_token;
                    var method_action = method(
                        params,
                        data,
                        function () {
                            //add additional code
                            if (!isBackground)
                                decrementRequest();
                            if (success) {
                                success.apply(null, arguments);
                            }
                        },
                        function (err) {
                            if (err.status == 401) {
                                //unauthorized, try log in again
                                var authHttp = $http({ method: 'GET', url: appConfig.siteUrl + '/token' });
                                authHttp.then(function (result) {
                                    if (result.status == 401) {
                                        window.location.href = appConfig.siteUrl + '/account/login';
                                        return;
                                    }
                                    
                                    localStorage.setItem('token', JSON.stringify(result.data));
                                    
                                    //authorization succeeded, repeat the request
                                    method(
                                        angular.extend({}, err.config.params || {}, { access_token: token().access_token }),
                                        err.config.data,
                                        function () {
                                            if (!isBackground)
                                                decrementRequest();
                                            
                                            if (success)
                                                success.apply(null, arguments);
                                        },
                                        function (err) {
                                            if (!isBackground)
                                                decrementRequest();
                                            
                                            if (err.status == 401) {
                                                console.error('The new token is invalid');
                                                window.location.href = appConfig.siteUrl + '/account/login';
                                            }
                                        }
                                    );
                                });
                            }
                            else if (err.status == 404) {
                                if (!isBackground)
                                    decrementRequest();
                                if (error) {
                                    error.apply(null, arguments);
                                }
                                $location.path('/404');
                            }
                            else {
                                if (!isBackground)
                                    decrementRequest();
                                if (error) {
                                    error.apply(null, arguments);
                                }
                            }
                        }
                    );
                    return method_action;
                };
            })(resource, action);
        });
        
        return resource;
    };
    
    var service = {
        requests: requests,
        isWorking: false,
        createResource: createResource,
        cancelAllRequests: cancelAllRequests,
        token: token
    };
    
    return service;
});



app.factory('blacklistService', function ($q, resourceService, appConfig) {
    return resourceService.createResource('/blacklists/:id/', { id: '@_id' });
});
app.factory('routerService', function ($q, resourceService, appConfig) {
    return resourceService.createResource(
        '/routers/:id/',
        { id: '@_id' },
        {
            connect: {
                method: 'GET',
                url: '/routers/:id/connect'
            },
            disconnect: {
                method: 'GET',
                url: '/routers/:id/disconnect'
            },
            eventsHandlerAdd: {
                method: 'GET',
                url: '/routers/:id/events/add'
            },
            eventsHandlerRemove: {
                method: 'GET',
                url: '/routers/:id/events/remove'
            },
        }
    );
});
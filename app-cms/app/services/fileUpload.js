﻿app.factory('fileUpload', function ($http) {
    var upload = function (file, uploadUrl, success) {
        var fd = new FormData();
        fd.append('content', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        })
        .success(function () {
            if (success)
                success();
        })
        .error(function () {
        });
    };
    return upload;
});
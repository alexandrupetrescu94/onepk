﻿app.factory('appConfig', function (serverConfig) {
    return angular.extend(serverConfig, {
        quietMillis: 400,
        errSrc: {
            router: {
                thumb: 'Images/Placeholders/router.png',
            },
            article: {
                big: 'Images/Placeholders/article_no_image1400x1050.png',
                thumb: 'Images/Placeholders/article_no_image200x200.png',
                picker: 'Images/Placeholders/article_no_image100x100.png',
                tag: 'Images/Placeholders/article_no_image50x50.png',
                media: 'Images/Placeholders/article_no_image300x200.png'
            },
            user: {
                big: 'Images/Placeholders/user_no_image1400x1050.png',
                thumb: 'Images/Placeholders/user_no_image200x200.png',
                picker: 'Images/Placeholders/user_no_image100x100.png',
                tag: 'Images/Placeholders/user_no_image50x50.png', 
                media: 'Images/Placeholders/user_no_image300x200.png',
                small: 'Images/Placeholders/user_no_image500x500.png'
            }
        },
        errSrcUrl: serverConfig.SiteUrl + '/no_image.png',
    });
});
﻿app.filter('moment', function () {
    return function (date, format) {
        if (!date) return "";
        return moment(date).format(format);
    };
});
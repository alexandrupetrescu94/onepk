﻿app.controller('masterController', function ($scope, notify, $socketIO, $location, $timeout, appConfig, resourceService) {
    
    $scope.resourceService = resourceService;
    $scope.isMenuVisible = true;
    $scope.page_size = 10;
    $scope.$on('toggleMenu', function (e, data) {
        e.preventDefault();
        $scope.isMenuVisible = !$scope.isMenuVisible;
    });

    $scope.isActive = function (path) {
        if (path instanceof Array)
            return path.filter(function (x) { $location.path().indexOf(x) != -1; }).length > 0;
        return ($location.path().indexOf(path) != -1);
    };
    
    $scope.search_term = '';
    $scope.search = function () {
        if ($scope.searchDelay)
            $timeout.cancel($scope.searchDelay);
        $scope.searchDelay = $timeout(function () {
            $scope.page = 1;
            $scope.$broadcast('search', $scope.search_term);
        }, appConfig.quietMillis);
    };
    $scope.appConfig = appConfig;
    
    $scope.init_menu = function () {
        $('#side-menu').metisMenu();
    };

    $socketIO.then(function(socket){
        if (!appConfig.socket || typeof appConfig.socket === 'undefined')
        {
            console.log(appConfig);
            socket.emit('subscribe', "pubsub");
            appConfig.socket = true;
            console.log(appConfig.socket);
        }
        socket.on('message', function(data){
            console.log(data);
            if (data.hasOwnProperty('message'))
                notify({ message: data.message, classes: ['alert-success', 'alert'] });
        });
    });
});
app.controller('configurationsController', function ($scope, $http, $route, $q, $location, notify, appConfig, configurationsService, resourceService) {
    
    var resource = configurationsService;
    var route = '/';
    var page = {};
    var details = {}; 
    //#region page
    page.fetch = function () {
    };

    details.fetch = function() {
      resource.get({},function(data, headers){details.item = data.configuration});
      
      $scope.details.save = function() {
        return $http({
          method: 'POST',
          url:  'http://localhost:5000/configurations/',
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token').access_token
          },
          data: {
            'configurations': details.item
          }
        }).then(
          function success(response){console.log(response.data.message);},
          function error(response){}
        );         
      };
    }

    //#endregion
    $scope.details = details;
    $scope.appConfig = appConfig;
    $scope.page = page;
});

app.controller('homeController', function ($scope, $route, $q, $location, notify, appConfig, homeService, resourceService) {
    
    var resource = homeService;
    var route = '/';
    var page = {};
    
    //#region page
    page.fetch = function () {
        $scope.details = resource.get({},function(){});
    };

    //#endregion
    $scope.appConfig = appConfig;
    $scope.page = page;
});

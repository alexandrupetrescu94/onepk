app.controller('routerController', function ($http, $scope, $route, $q, $location, notify, appConfig, lodash, routerService, resourceService) {
    var _ = lodash;
    var resource = routerService;
    var list = {};
    var details = {};
    var singleRoute = '/routers/';
    var pluralRoute = '/routers/';


    list.items = [];
    list.search_term = null;

    list.newRouter = function () {
        $location.path(singleRoute + 'new');
    }

    list.select = function (item) {
        $location.path(singleRoute + item._id);
    };

    list.delete = function (item) {
        if (confirm('Sunteti sigur ca doriti stergerea ?')) {
            console.log(item);
            $http({
                method: 'DELETE',
                url:  'http://localhost:5000/routers/' + item._id + '/',
                headers: {
                  Authorization: 'Bearer ' + localStorage.getItem('token').access_token
                }
            }).then(
            function success(response){
                console.log(response.data.message);
                notify({ message: 'S-a sters cu success', classes: ['alert-success', 'alert'] });
                list.fetch();
            },
            function error(response){}); 
        }
    }

    list.events = function (item, eventsStatus) {
        if ( item.status === 'on' )
        {//alert your router onepk connection is off
            if (eventsStatus === 'on' && item.events === 'off') {
                resource.eventsHandlerAdd({ id: item['_id'] }, function (data, headers){
                    _.merge(item,data.router);
                });
            } 
            else 
            if (eventsStatus === 'off' && item.events === 'on') {
                resource.eventsHandlerRemove({ id: item['_id'] }, function (data, headers){
                    _.merge(item,data.router);
                });
            }
        } else {
            return notify({ message: 'Please open onePK Connection', classes: ['alert-danger', 'alert'] });
        }
    }
    
    list.status = function (item, onepkStatus) {
        if (onepkStatus === 'on' && item.status === 'off') {
            resource.connect({ id: item['_id'] }, function (data, headers){
                _.merge(item,data.router);
            });
        }
        else 
        if (onepkStatus === 'off' && item.status === 'on') {
            if (item.events === 'on')
                resource.eventsHandlerRemove({ id: item['_id'] });

            resource.disconnect({ id: item['_id'] }, function (data, headers){
                _.merge(item,data.router);
            });
        }
        return;
    }

    list.fetch = function () {
        resource.get({}, function (data, headers) {
            list.items = data.routers;
        });
    };
    //#endregion

    //#region details
    details.fetch = function () {
        details.id = $route.current.params.id;
        details.isNew = details.id == 'new';
        details.item = details.isNew ? new resource({}) : resource.get({ id: details.id },function(data, headers){
            details.item.duplicate = _.cloneDeep(details.item.onepkRouter)
            console.log(details.item);
            details.cli = [details.item.db_router['name'] + '#'];
            details.cliCommand = '';
            details.cliLabel=details.item.db_router['name'] + '#';
            details.options = {
                chart: {
                    type: 'lineChart',
                    height: 450,
                    margin : {
                        top: 20,
                        right: 20,
                        bottom: 40,
                        left: 55
                    },
                    x: function(d){ return d.x; },
                    y: function(d){ return d.y; },
                    useInteractiveGuideline: true,
                    dispatch: {
                        stateChange: function(e){ console.log("stateChange"); },
                        changeState: function(e){ console.log("changeState"); },
                        tooltipShow: function(e){ console.log("tooltipShow"); },
                        tooltipHide: function(e){ console.log("tooltipHide"); }
                    },
                    xAxis: {
                        axisLabel: 'Time (min)',
                        tickFormat: function(d) {
                            return d3.time.format('%c')(new Date(d))
                        },
                    },
                    yAxis: {
                        axisLabel: 'Interface Metrics (M)',
                        tickFormat: function(d){
                            return d3.format('.02f')(d);
                        },
                        axisLabelDistance: -10
                    },
                    callback: function(chart){
                        console.log("!!! lineChart callback !!!");
                    },
                    zoom: {
                        enabled: true,
                        scaleExtent: [1, 10],
                        useFixedDomain: false,
                        useNiceScale: false,
                        horizontalOff: false,
                        verticalOff: true,
                        unzoomEventType: 'dblclick.zoom'
                    }
                },
            };
            details.data =  [
                {
                    values: details.item.stats.load,      //values - represents the array of {x,y} data points
                    key: 'Router Load', //key  - the name of the series.
                    color: '#ff7f0e',  //color - optional: choose your own line color.
                    strokeWidth: 2,
                    classed: 'dashed'
                },
                {
                    values: details.item.stats.reliability,
                    key: 'Router Reliability',
                    color: '#2ca02c'
                },
                {
                    values: details.item.stats.usage,
                    key: 'Router Usage',
                    color: '#7777ff',
                    area: true      //area - set to true if you want this line to turn into a filled area chart.
                }
            ];
        });
        
        details.checkConnection = function () {
            console.log(details.newIntIp);
            $http({
                method: 'POST',
                url:  'http://localhost:5000/routers/new',
                headers: {
                  Authorization: 'Bearer ' + localStorage.getItem('token').access_token
                },
                data: {
                    'ip_address': details.newIntIp,
                    'interface': details.newInt,
                    'name': details.item.db_router.name,
                    'description': details.item.db_router.description ? details.item.db_router.description : '',
                }
            }).then(
            function success(response){
                console.log(response);
                if (response.data.message == "Error")
                    return notify({ message: 'A aparut o eroare la conectare, reincercati', classes: ['alert-danger', 'alert'] });
                else
                    $location.path(singleRoute + response.data.id);
            },
            function error(response){}); 
        };

        details.sendCommand = function() {
            if (details.cliCommand !== '') {
                $http({
                    method: 'POST',
                    url:  'http://localhost:5000/routers/' + details.item.db_router['_id'] + '/cli',
                    headers: {
                      Authorization: 'Bearer ' + localStorage.getItem('token').access_token
                    },
                    data: {
                        'command': details.cliCommand,
                    }
                }).then(
                function success(response){
                    if (response.data.message.scope !== "Error")
                    {
                        details.cli.push(details.cliLabel + " " + details.cliCommand)
                        details.cliLabel = response.data.message.scope;
                    } else
                        details.cli.push("Bad Command")
                    if (details.cli.length > 10)
                        details.cli.shift();
                    details.cliCommand = ''
                },
                function error(response){}); 
            } else {
                details.cli.push(details.item.db_router['name'] + '#')
            }
        }
        details.save = function () {
            if (!$scope.mainForm.$valid) {
                return notify({ message: 'Nu au fost completate toate campurile', classes: ['alert-danger', 'alert'] });
            }
            routerIfs = [];
            details.item['onepkRouter'].forEach(function(ifs, index){
                if (ifs['ip_address'] !== '' 
                    && ifs['interface'] != details.item['db_router']['interface']
                    && ifs['ip_address'] != details.item['db_router']['ip'])
                    {
                        routerIfs.push(ifs);
                        routerIfs[index]['oldIp'] = details.item.duplicate[index]['ip_address'];
                        routerIfs[index]['oldPrefix'] = details.item.duplicate[index]['prefix'];
                    }
            });
            $http({
                method: 'POST',
                url:  'http://localhost:5000/routers/' + details.item.db_router['_id'] + '/',
                headers: {
                  Authorization: 'Bearer ' + localStorage.getItem('token').access_token
                },
                data: {
                    'name': details.item['db_router'].name,
                    'description': details.item['db_router'].description,
                    'interfaces': routerIfs
                }
            }).then(
                function success(response){
                    $location.path(pluralRoute);
                },
                function error(response){}
            ); 

        };
        
        details.state = function (index, intf, state){
            $http({
                method: 'POST',
                url:  'http://localhost:5000/routers/' + details.item.db_router['_id'] + '/if_state',
                headers: {
                  Authorization: 'Bearer ' + localStorage.getItem('token').access_token
                },
                data: { 'element': intf, 'state': state }
            }).then(
                function success(response){
                    details.item.onepkRouter[index].state = response.data.state;
                },
                function error(response){}
            ); 
        };

        details.baseConfig = function (){
            $http({
                method: 'POST',
                url:  'http://localhost:5000/routers/' + details.item.db_router['_id'] + '/base_config',
                headers: {
                  Authorization: 'Bearer ' + localStorage.getItem('token').access_token
                }
            }).then(
                function success(response){
                    details.item.onepkRouter[index].state = response.data.state;
                },
                function error(response){}
            ); 
        }

        details.cancel = function () {
            $location.path(pluralRoute);
        };
    };

    //#endregion
    $scope.appConfig = appConfig;
    $scope.now = new Date().getTime();
    $scope.list = list;
    $scope.details = details;
    $scope.file = '';
    $scope.files = [];
});

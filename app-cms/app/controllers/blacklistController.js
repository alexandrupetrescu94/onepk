app.controller('blacklistController', function ($scope, $http, $route, $q, $location, notify, appConfig, blacklistService, resourceService) {
    
    var resource = blacklistService;
    var route = '/';
    var page = {};
    var list = [];
    var levels = [1,2,3];
    $scope.levels = levels;
   
    //#region page
    page.fetch = function () {
    };

    list.fetch = function () {
      $scope.details = resource.get({},function (data, headers) {
          list.records = headers()['x-records'];
          list.total_pages = headers()['x-total-pages'];
          list.pages = [];

          var from = list.page > 3 ? list.page - 3 : 1;
          var toAdd = from == 1 ? 5 : 2;
          var to = 0;
          if (from == 1)
              to = 6 > list.total_pages ? list.total_pages : 6;
          else
              to = list.page + toAdd > list.total_pages ? list.total_pages : list.page + toAdd;
          for (var i = from; i <= to; i++) {
              list.pages.push(i);
          }
          list.items = data.blacklists;
          if (list.items.length == 0)
              list.prevPage();
        });
      
    };

    list.updateLevel = function (item) {
      // update alert level
      return $http({
        method: 'POST',
        url:  'http://localhost:5000/blacklists/' + item['_id'] + '/level',
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token').access_token
        },
        data: {
          'level':item.level
        }
      }).then(
        function success(response){},
        function error(response){}
      );        
    };

    list.updateEmail = function(item,status){
      if (item.email != status) {   
        // update alert to email
        return $http({
          method: 'POST',
          url:  'http://localhost:5000/blacklists/' + item['_id'] + '/email',
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token').access_token
          },
          data: {
            'email':status
          }
        }).then(
        function success(response){
          console.log(response.data.item.ok);
          if (response.data.item.ok === 1)
            item.email = status;
        },
        function error(response){console.log(response);}
        );         
      }
    };

    //#endregion
    $scope.list = list;
    $scope.appConfig = appConfig;
    $scope.page = page;
});

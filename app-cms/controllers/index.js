var express = require('express');
var config = require('../config.js');
var router = express.Router();
var isLoggedIn = function (req, res, next) {
    if (!req.session.access_token)
        return res.redirect('/account/login');
    next();
};

/*router.get('/', isLoggedIn, function (req, res, next) {
    res.render('index', { release: config.isRelease });
});*/
router.get('/', function (req, res, next) {
    res.render('index', { release: config.isRelease });
});
//make server config available to angular app
/*router.get('/config', isLoggedIn, function (req, res, next) {
    res.send('angular.module("serverModule", []).constant("serverConfig", ' + JSON.stringify({
        apiUrl: config.apiUrl,
        siteUrl: config.siteUrl,
        isRelease: config.isRelease
    }) + ');');
});*/
router.get('/config', function (req, res, next) {
    res.send('angular.module("serverModule", []).constant("serverConfig", ' + JSON.stringify({
        apiUrl: config.apiUrl,
        siteUrl: config.siteUrl,
        isRelease: config.isRelease
    }) + ');');
});

module.exports = router;

var express = require('express');
var config = require('../config.js');
var inspector = require('schema-inspector');
var accountModel = require('../models/accountModel.js');
var request = require('request');
var moment = require('moment');
var router = express.Router();

router.get('/login', function (req, res, next) {
    res.render('login', {});
});

router.post('/login', function (req, res, next) {
    var model = req.body;
    
    var modelState = inspector.validate(accountModel, model);
    if (modelState.valid) {
        inspector.sanitize(accountModel, model);
        
        request.post(config.loginEndpoint, {
            headers: {
                Authorization: 'Basic ' + new Buffer(config.appKey + ':' + config.appSecret).toString('base64')
            },
            form: {
                grant_type: 'password',
                username: model.username,
                password: model.password
            }
        }, function (error, response, body) {
            if (error || response.statusCode != 200) {
                return res.render('login', { error: [{ message: 'Utilizatorul sau parola sunt incorecte' }] });
            }
            
            //success
            //store in memory for token reissue
            req.session.username = model.username;
            req.session.password = model.password;
            req.session.access_token = JSON.parse(body);
            req.session.access_token.expires = moment().add('seconds', parseInt(req.session.access_token.expires_in)).toDate().getTime();
            return res.redirect('/');
        });
    } else {
        return res.render('login', modelState);
    }
});

router.get('/logout', function (req, res, next) {
    req.session.destroy();
    res.redirect('/account/login');
});

module.exports = router;

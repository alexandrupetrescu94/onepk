var express = require('express');
var router = express.Router();
var config = require('../config.js');
var moment = require('moment');
var request = require('request');

router.get('/', function (req, res, next) {
    if (!req.session.access_token)
        return res.status(401).json({ message: 'not logged in' });
    next();
}, function (req, res, next) {
    var token = req.session.access_token;
    
    if (token.expires >= new Date().getTime())
        return res.json(token);
    
    //obtain token
    request.post(config.tokenEndpoint, {
        headers: {
            Authorization: 'Basic ' + new Buffer(config.appKey + ':' + config.appSecret).toString('base64')
        },
        form: {
            grant_type: 'password',
            username: req.session.username,
            password: req.session.password
        }
    }, function (error, response, body) {
        if (error || response.statusCode != 200) {
            return res.status(401).json({ message: 'token could not be obtained' });
        }
        
        //success
        req.session.access_token = JSON.parse(body);
        req.session.access_token.expires = moment().add('seconds', parseInt(req.session.access_token.expires_in)).toDate().getTime();
        return res.json(req.session.access_token);
    });
});

module.exports = router;
var express = require('express');
var session = require('express-session');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config.js');
var redis = require('redis');
var qs = require('querystring');
var debug = require('debug')('onepk-cms:server');
var http = require('http');


var app = express();
app.use(session({
    resave: false, // don't save session if unmodified
    saveUninitialized: false, // don't create session until something stored
    secret: '51814245277520219831'
}));

console.log('Started on PORT ' + (process.env.PORT || 3000));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', require('hbs').__express);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/app', express.static(path.join(__dirname, 'app')));

app.use('/', require('./controllers/index'));
app.use('/account', require('./controllers/account'));
app.use('/token', require('./controllers/token'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: err
    });
});

app.set('port', '3000');


var server = http.createServer(app);
server.listen('3000');

var io = require('socket.io')(server);
var first = true;

io.sockets.on('connection', function(socket){
  
  var client = redis.createClient();

  socket.on('subscribe', function (channel){
      client.subscribe(channel);
      if (first)
      {
        socket.emit("message", {'message': "Real-time notifications available"});
        first = false;
      }
  });

  socket.on('unsubscribe', function (obj) {
      subscribe.unsubscribe(obj.channel);
  });

  
  client.on("message",function(channel, realTimeData){
    var rt = JSON.parse(realTimeData);
    if (rt.hasOwnProperty('message') && rt.hasOwnProperty('ip_address'))
    { 
      socket.emit('message',{
          'channel': channel,
          'message': rt.message,
          'ipAddress': rt.ip_address,
      });
    } else {
      socket.emit('message',{
          'channel': channel,
          'message': 'just test ' + rt,
          'ipAddress': 'test',
      });      
    }

  });
});
var inspector = require('schema-inspector');

var schema = {
    strict: true,
    type: 'object',
    properties: {
        username: {
            type: 'string',
            ne: '',
            optional: false,
            rules: ['trim', 'lower'],
            code: 'Utilizator',
            error: 'Campul este obligatoriu'
        },
        password: {
            type: 'string',
            ne: '',
            optional: false,
            code: 'Parola',
            error: 'Campul este obligatoriu'
        }
    }
};

module.exports = schema;
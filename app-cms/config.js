var config = {};
var port = process.env.PORT || 3000;

config.appKey = '2c234cafdea643b23a23d0412abdf981';
config.appSecret = '0081b3a82a46152389eafeb38124dddd';

config.dbConnectionString = 'mongodb://localhost/appserver';

config.isRelease = false;

config.signupEndpoint = 'http://localhost:5000/api/signup';
config.loginEndpoint = 'http://localhost:5000/api/login';
config.tokenEndpoint = 'http://localhost:5000/api/token';

config.apiUrl = 'http://localhost:5000';
config.siteUrl = 'http://localhost:' + port;
config.socket = false;

module.exports = config;
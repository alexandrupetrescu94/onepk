from onep.element.NetworkElement import NetworkElement  
from onep.element.SessionConfig import SessionConfig  
from onep.element.NetworkApplication import NetworkApplication
from onep.interfaces import InterfaceFilter, InterfaceStatus, NetworkInterface, InterfaceStateListener
from onep.core.util import  OnepConstants, enum, tlspinning
from onep.core.exception import OnepConnectionException
from onep.interfaces import NetworkPrefix, InterfaceStatisticsListener, InterfaceStatistics, InterfaceStatisticsFilter
from onep.routing import ARTRouteStateListener
from onep.routing import AppRouteTable
from onep.routing import L3UnicastNextHop
from onep.routing import L3UnicastRIBFilter
from onep.routing import L3UnicastRoute
from onep.routing import L3UnicastRouteOperation
from onep.routing import L3UnicastRouteRange
from onep.routing import L3UnicastScope
from onep.routing import RIB
from onep.routing import RIBRouteStateEvent
from onep.routing import RIBRouteStateListener
from onep.routing import ReplayRouteEventListener
from onep.routing import RouteRange
from onep.routing import Routing
from onep.routing import UpdateRouteResponseListener
from api.oauth import authModels
from onep.vty.VtyService import VtyService

from datetime import datetime
import logging
import time
import json
import smtplib
import socket
import struct

from scripts import redisHandler
from scripts.pinningHandler import PinningHandler

# Network Application
network_application = NetworkApplication.get_instance()
network_application.name = 'onepk.py'
logger = logging.getLogger('onep:VTYTutorial')
logger.setLevel(logging.INFO)
testRedis = []

# Setup a connection config with TSL pinning handler
def one_pk_config():
    config = SessionConfig(None)  
    config.set_tls_pinning('', PinningHandler(''))  
    config.transportMode = SessionConfig.SessionTransportMode.TLS  
    return config;

# RouterAuthConfiguration
config = one_pk_config()

class MyInterfaceStatisticsListener(InterfaceStatisticsListener):
    name = str()

    def __init__(self, name):        
        super(MyInterfaceStatisticsListener, self).__init__()
        self.name = name
    
    """
      Invoked when an event is received from a network element.
      @param event
                 An event object that indicates that an event has occurred in a network element.
      @param clientData
                 The clientData is an object that is passed in when the application calls an API to add/register
                 the event listener. The application is responsible for casting the input clientData to the
                 appropriate class before using it.
    """     
    def handle_event(self, event, clientData):            
            print self.name + " has received statistics change event - from Event Handler "
            if InterfaceStatisticsParameter.ONEP_IF_STAT_RX_PKTS_BCAST == event.parameter:
                print "\tchange in ONEP_IF_STAT_RX_PKTS_BCAST value on interface " + event.interface.name

# Interface State Listener
class MyInterfaceStateListener(InterfaceStateListener):
    ip_address = str()

    def __init__(self, ip_address):        
        super(MyInterfaceStateListener, self).__init__()
        self.ip_address = ip_address
        print ip_address

    def handle_event(self, event, clientData):   
        message = 'State change event'
        # router = authModels.router.find_by_id(some_id)
        # router.update({'events': message})
        if (event.interface_state == 1):
            testRedis.append(datetime.utcnow())
            print 'TEST REDIS LENGTH'
            print len(testRedis)
            notification = authModels.notification({
                "type": "interface_shut_down",
                "ip": self.ip_address           
            })
            notification.validate()
            notification.save()
            redisHandler.get().publish('pubsub',json.dumps(
                {
                    "ip_address": self.ip_address,
                    "message": "Router with Ip Address %s has been shuted down" % datetime.utcnow()
                }
            ))
            blacklist = authModels.blacklist.find_one({'name': "Interface_Shut_Down",})
            if (blacklist['email'] == 1):
                server = smtplib.SMTP('smtp.gmail.com', 587)
                server.starttls()
                server.login("bitpk.contact@gmail.com", "flaskonepk")
                msg = {}
                subject = 'BitPK Alert'
                msg['Subject'] = 'Subject: %s\n\n Router with Ip Address %s shuted down' % (subject, self.ip_address)
                msg['From'] = "bitpk.contact@gmail.com"
                msg['To'] = "alexandru.petrescu@my.fmi.unibuc.ro"
                server.sendmail(msg['From'], [msg['To']], msg['Subject'])
                server.quit()

class Router:
    def get_router(self):
        return self.router #network_application.get_network_element(ip_address)

    # Router Connect with the credentials
    def connect_router(self,ip_address, username, password):
        # username and password are set to onepk and cisco with privilege 15 on router
        self.router = network_application.get_network_element(ip_address)
        print self.router
        self.eventHandle = None
        if self.router == None:
            logger.error("Failed to get network element")
            return "Error"
        self.ip_address = ip_address
        try:
            x=self.router.connect(username, password, config)
            print x
            self.vtyService = VtyService(self.router)
            self.vtyService.open()
            self.get_router_active_interfaces()
            print 'Router with ' + ip_address + ' is connected.'
            return "Connected"
        except Exception, e:
            logger.error("Failed to connect: %s", e)
            return "Error"
        return "Error"
        

    def disconnect_router(self, ip_address):
        self.router.disconnect()
        print 'Router with ' + ip_address + ' is connected.'

    # Get IP and Subnet Prefix for being added to the interfaces dictionary array 
    # Used by get_router_active_interfaces
    def get_interface_ip_subnet_mask(self,interface):
        interfaceList = NetworkInterface.get_prefix_list(interface)
        return str(interfaceList[0]).split()[1::2] if not(len(interfaceList) == 0) else ['','']

    def get_router_interfaces(self):
        if_types = NetworkInterface.InterfaceTypes
        if_status = InterfaceStatus.InterfaceState
        if_filter = InterfaceFilter(None,if_types.ONEP_IF_TYPE_ANY)
        self.all_interfaces = []
        for interface in self.router.get_interface_list(if_filter):
            int_config = NetworkInterface.get_config(interface)
            ip_subnet_arr = self.get_interface_ip_subnet_mask(interface)
            self.all_interfaces.append({
                'interface'   : int_config.display_name,
            #    'mac_address' : int_config.mac_address,
                'mtu'         : str(int_config.mtu),
                'ip_address'  : ip_subnet_arr[0],
                'prefix'      : ip_subnet_arr[1],
                'state': interface.get_status().link
            })
        return self.all_interfaces;

    # Router Active Interfaces
    def get_router_active_interfaces(self):
        if_types = NetworkInterface.InterfaceTypes
        if_status = InterfaceStatus.InterfaceState
        if_filter = InterfaceFilter(None,if_types.ONEP_IF_TYPE_ANY)
        self.interfaces = []

        for interface in self.router.get_interface_list(if_filter):
            int_status = interface.get_status()
            if (int_status.link == if_status.ONEP_IF_STATE_OPER_UP and int_status.lineproto == if_status.ONEP_IF_STATE_OPER_UP):
                int_config = NetworkInterface.get_config(interface)
                ip_subnet_arr = self.get_interface_ip_subnet_mask(interface)
                self.interfaces.append({
                    'interface'   : int_config.display_name,
                #    'mac_address' : int_config.mac_address,
                    'mtu'         : str(int_config.mtu),
                    'ip_address'  : ip_subnet_arr[0],
                    'prefix'      : ip_subnet_arr[1] 
                })

        return self.interfaces;

    def events_router(self):
        InterfaceStateEventType = InterfaceStatus.InterfaceStateEventType
        elementStateListener = MyInterfaceStateListener(self.ip_address)
        ifFilter = InterfaceFilter()
        print "Adding Interface State Change event listener on network element "
        self.eventHandle = self.router.add_interface_state_listener(
                elementStateListener,
                ifFilter,
                InterfaceStateEventType.ONEP_IF_STATE_EVENT_LINK,
                'You Can Add Client Data on events_router function'
            )
        
    def events_remove_router(self):
        self.router.remove_interface_state_listener(self.eventHandle)

    def statistics_router(self):
        interface = self.router.get_interface_by_name('GigabitEthernet0/1')
        ifsLength = len(self.interfaces)
        avgLoad = 0.0
        avgReliability = 0.0
        for ifs in self.interfaces:
            statistics = interface.get_statistics()
            avgLoad += statistics.transmit_load
            avgReliability += statistics.reliability
        avgLoad /= ifsLength
        avgReliability /= ifsLength
        
        notification = authModels.notification({
            "type": "stats",
            "ip": self.ip_address,
            "load": float(avgLoad),
            "reliability": float(avgReliability),
            "usage": float(self.get_element_process_attributes() / 1.0),
        })
        notification.validate()
        notification.save()

        return {
            'load': avgLoad / 1.0,
            'reliability': avgReliability / 1.0,
            'usage': self.get_element_process_attributes() / 1.0
        }
        #reliability < 200
        #bandwidth > 150
        #Per router: max(allocatedMemory / freedMemory, cpuUsage) < 80
        #return statistics

    def send_commands(self, cmdList):
        print cmdList
        cli_result = []
        for cmd in cmdList:
            cli_result.append(self.vtyService.write(cmd))
        self.vtyService.write('end')
        return cli_result

    def send_cli_command(self, cmd):
        print cmd
        self.vtyService.write(cmd)
        parser_state = self.vtyService.get_parser_state()
        logger.info("ParserState prompt - %s", parser_state.prompt)
        logger.info("ParserState overallrc - %s", parser_state.overallRC) #error if 1 / 0 if command is ok
        return {"scope": parser_state.prompt} if not parser_state.overallRC else {"scope":"Error"}

    def get_element_process_attributes(self):
        for element_process in self.router.get_process_list():
            allocatedMemory = 0
            cpuUsage = 0
            freedMemory = 0
            allocatedMemory += element_process.allocatedMemory
            cpuUsage += element_process.cpuUsage
            freedMemory += element_process.freedMemory
        return max(allocatedMemory / freedMemory, cpuUsage)

routers = []

def connect(ip_address, username, password):
    router = Router()
    if (router.connect_router(ip_address, username, password) == "Connected"):
        routers.append(router)
        print routers
        return "Connected"
    return "Connection Error"
    
def properties(ip_address):
    for r in routers:
        if r.ip_address == ip_address:
            interfaces = r.get_router_active_interfaces()
            return interfaces

def disconnect(ip_address):
    disconnectedRouterIndex = None
    for idx, r in enumerate(routers):
        if r.ip_address == ip_address:
            r.disconnect_router(ip_address)
            disconnectedRouterIndex = idx
    if (not disconnectedRouterIndex == None):
        routers.pop(disconnectedRouterIndex)

def events_add(ip_address):
    for r in routers:
        if r.ip_address == ip_address:
            r.events_router()

def events_remove(ip_address):
    for r in routers:
        if r.ip_address == ip_address:
            r.events_remove_router()

def cidr(prefix):
    return socket.inet_ntoa(struct.pack(">I", (0xffffffff << (32 - int(prefix))) & 0xffffffff))

def reverse_cidr(prefix):
    return socket.inet_ntoa(struct.pack(">I", (0xffffffff >> (int(prefix))) & 0xffffffff))

def check_connection(ip_address, username, password):
    router = Router()
    print router.connect_router(ip_address, username, password)
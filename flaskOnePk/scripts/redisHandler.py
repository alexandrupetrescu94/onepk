import redis

def run():
	global r
	try:
		r = redis.StrictRedis(host='localhost',port=6379,db=0)
		r.ping()
	except redis.ConnectionError:
		r = None
		print "WebServer Connection not available but the app still can work"

def get():
	return r
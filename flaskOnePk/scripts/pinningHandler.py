from onep.core.util import tlspinning

# TLS Connection (This is the TLS Pinning Handler)  
class PinningHandler(tlspinning.TLSUnverifiedElementHandler):  
    def __init__(self, pinning_file):  
        self.pinning_file = pinning_file  
    def handle_verify(self, host, hashtype, finger_print, changed):  
        return tlspinning.DecisionType.ACCEPT_ONCE  

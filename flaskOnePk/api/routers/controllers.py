from flask import jsonify, Blueprint, Response, Request, request
from scripts import onepk
from scripts import redisHandler
from api.oauth import token
from api.oauth import authModels
from bson import ObjectId
import time

routers = Blueprint('routers', __name__)

@routers.route('/', methods=['GET','OPTIONS'])
@token.authorize(scopes = ['app_scope'])
def index(rejected):
	if (rejected): #if error on token authorization
		return rejected
	cursor = authModels.router.find();
	routers = list()
	for c in cursor:
		c['_id'] = str(c['_id'])
		routers.append(c)
	return jsonify({"routers":routers})
	#Can it be done? : do you have any routers? else scan topology

@routers.route('/<string:router_id>/connect', methods=['GET'])
def connect_router(router_id):
	print 'connect'
	username = 'onepk'
	password = 'cisco'
	router = authModels.router.find_one({'_id': ObjectId(router_id)})
	onepk.connect(router['ip'], username, password)
	router['status'] = "on"
	router.save()
	router['_id'] = str(router['_id'])
	return jsonify({'message':'Router connected', 'router':router})

@routers.route('/<string:router_id>/properties', methods=['GET'])
def properties_router(router_id):
	ip = '192.168.20.1' + str(router_id)
	interfaces = onepk.properties(router['ip'])
	return jsonify({'message':interfaces[:]})

@routers.route('/<string:router_id>/events/add', methods=['GET'])
def events_add_router(router_id):
	router = authModels.router.find_one({'_id': ObjectId(router_id)})
	onepk.events_add(router['ip'])
	router['events'] = "on"
	router['statistics'] = "Good" #todo calculate statistics from router page
	router.save()
	router['_id'] = str(router['_id'])
	return jsonify({'message': 'Handler added', 'router':router})

@routers.route('/<string:router_id>/events/remove', methods=['GET'])
def events_remove_router(router_id):
	router = authModels.router.find_one({'_id': ObjectId(router_id)})
	onepk.events_remove(router['ip'])
	router['events'] = "off"
	router['statistics'] = "Offline"
	router.save()
	router['_id'] = str(router['_id'])
	return jsonify({'message': 'Handler removed', 'router':router})

@routers.route('/<string:router_id>/disconnect', methods=['GET'])
def disconnect_router(router_id):
	router = authModels.router.find_one({'_id': ObjectId(router_id)})
	if (router['events'] == "on"):
		onepk.events_remove(router['ip'])
	onepk.disconnect(router['ip'])
	router['status'] = "off"
	router.save()
	router['_id'] = str(router['_id'])
	return jsonify({'message':'Router disconnected', 'router':router})

@routers.route('/<string:router_id>/', methods=['GET'])
def get_router(router_id):
	router = authModels.router.find_one({'_id': ObjectId(router_id)})
	router['_id'] = str(router['_id'])
	for r in onepk.routers:
		if (r.ip_address == router['ip']):
			onepk_router = r
	onepk_router.statistics_router()
	cursor = authModels.notification.find({'type':'stats'}).sort('datetime',-1).limit(5)
	stats = {'load': None, 'reliability': None, 'usage': None}
	stats['load'] = list()
	stats['reliability'] = list()
	stats['usage'] = list()
	i = 0
	for c in cursor:
		tounix = 1000 * time.mktime(c['datetime'].timetuple())
		stats['load'].append({'x':tounix, 'y':c['load']})
		stats['reliability'].append({'x':tounix, 'y':c['reliability']})
		stats['usage'].append({'x':tounix, 'y':c['usage']})
		i += 1
	return jsonify({'db_router':router, 'onepkRouter': onepk_router.get_router_interfaces(), 'stats':stats})

@routers.route('/<string:router_id>/', methods=['DELETE'])
def delete_router(router_id):
	router = authModels.router.find_one({'_id': ObjectId(router_id)})
	onepk.disconnect(router['ip'])
	router.remove()
	return jsonify({'message':'ok'})


@routers.route('/<string:router_id>/', methods=['PUT','POST'])
def post_router(router_id):
	router = authModels.router.find_one({'_id': ObjectId(router_id)})
	for r in onepk.routers:
		if (r.ip_address == router['ip']):
			onepk_router = r
	data = request.get_json()
	authModels.router.update({
		'_id': ObjectId(router['_id'])
	},{
		'$set':{
			'description': data.get('description'),
			'name': data.get('name')
		}
	})

	cmd = [
		"en",
		"conf t",
		"hostname  " + data.get('name')
	]
	for interface in data.get('interfaces'):
		cmd.append("int " + interface['interface'])
		cmd.append("ip address " + interface['ip_address'] + " " + onepk.cidr(int(interface['prefix'])))
		cmd.append("mtu " + interface['mtu'])
		cmd.append("conf t")
		cmd.append("router ospf 1")
		if (interface['oldIp']):
			cmd.append("no network " + interface['oldIp'] + " " + onepk.reverse_cidr(int(interface['oldPrefix'])) + " area 0")
		cmd.append("network " + interface['ip_address'] + " " + onepk.reverse_cidr(int(interface['prefix'])) + " area 0")
	print '\n'
	print cmd
	print '\n'
	onepk_router.send_commands(cmd)
	return jsonify({'message':'ok'})

@routers.route('/<string:router_id>/if_state', methods=['PUT','POST'])
def router_state(router_id):
	router = authModels.router.find_one({'_id': ObjectId(router_id)})
	for r in onepk.routers:
		if (r.ip_address == router['ip']):
			onepk_router = r
	data = request.get_json()
	cmd = [
		"en",
		"conf t",
		"int " + data.get('element')['interface'],
		"shut" if (data.get('state') == 1) else "no shut"
	]
	result = onepk_router.send_commands(cmd)
	print result
	return jsonify({'message':'ok','state': data.get('state')})

@routers.route('/<string:router_id>/base_config', methods=['PUT','POST'])
def router_base_config(router_id):
	router = authModels.router.find_one({'_id': ObjectId(router_id)})
	for r in onepk.routers:
		if (r.ip_address == router['ip']):
			onepk_router = r
	configuration = authModels.configuration.find_one({"main":True})
	return jsonify({'message':onepk_router.send_commands(configuration['configArray'])})

@routers.route('/<string:router_id>/cli', methods=['PUT','POST'])
def router_cli(router_id):
	router = authModels.router.find_one({'_id': ObjectId(router_id)})
	for r in onepk.routers:
		if (r.ip_address == router['ip']):
			onepk_router = r
	return jsonify({'message':onepk_router.send_cli_command(request.get_json().get('command'))})

@routers.route('/new', methods=['PUT','POST'])
def router_new():
	username = 'onepk'
	password = 'cisco'
	router_ip = request.get_json().get('ip_address')
	router_exists = authModels.router.find({'ip': router_ip}).count()
	if (router_exists > 0):
		print "Router already exists"
		router = authModels.router.find_one({'ip': router_ip})
		onepk.connect(router['ip'], username, password)
		authModels.router.update({
			'_id': ObjectId(router['_id'])
		},{
			'$set':{
				'status': "on"
			}
		})
		return jsonify({'id':str(router['_id'])})

	newRouter = authModels.router({
		"name": request.get_json().get('name'),
		"ip": router_ip, 
		"interface":request.get_json().get('interface'), 
		"description": request.get_json().get('description'),
		"events": "off",
		"status":"on",
		"statistics":"Offline"
	})

	newRouter.validate()
	if (onepk.connect(router_ip, username, password) == "Connected"):
		newRouter.save()
		router = authModels.router.find_one({'ip': router_ip})
		for r in onepk.routers:
			if (r.ip_address == router['ip']):
				onepk_router = r
		onepk_router.send_commands([
			"en",
			"conf t",
			"hostname " + request.get_json().get('name'),
		])
		return jsonify({'id':str(router['_id'])})
	return jsonify({'message':"Error"})

@routers.route('/<string:router_id>/statistics', methods=['PUT','POST'])
def router_statistics(router_id):
	router = authModels.router.find_one({'_id': ObjectId(router_id)})
	for r in onepk.routers:
		if (r.ip_address == router['ip']):
			onepk_router = r
	return jsonify({'stats':onepk_router.statistics_router()})

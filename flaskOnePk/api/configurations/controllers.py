from flask import jsonify, Blueprint, Response, Request, request
from scripts import onepk
from scripts import redisHandler
from api.oauth import token
from api.oauth import authModels
from bson import ObjectId

import smtplib

configurations = Blueprint('configurations', __name__)

@configurations.route('/', methods=['GET','OPTIONS'])
@token.authorize(scopes = ['app_scope'])
def index(rejected):
	if (rejected): #if error on token authorization
		return rejected
	configuration = authModels.configuration.find_one({"main":True})
	return jsonify({"configuration":configuration['configArray']})

@configurations.route('/', methods=['PUT','POST'])
def configurations_level():
	data = request.get_json()
	configurations = authModels.configuration.find({"main":True})
	for c in configurations:
		authModels.configuration.update({
			'_id': ObjectId(c['_id'])
		},{
			'$set':{
				'main':False
			}
		})
	print type(data.get('configurations')[0])
	print data.get('configurations')[0]
	newConfig = authModels.configuration({
		"configArray": data.get('configurations'),
		"main":True
		})
	newConfig.validate()
	newConfig.save()

	return jsonify({'message':'ok'})

from flask import jsonify, Blueprint
from api.oauth import token

main = Blueprint('main', __name__)

@main.route('/', methods=['GET'])
@token.authorize(scopes = ['app_scope'])
def index(rejected):
	if (rejected): #if error on token authorization
		return rejected
	return jsonify({"message":"Main"})
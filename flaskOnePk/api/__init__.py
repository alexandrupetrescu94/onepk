from flask import Flask, Response, Request, make_response, request
from api.main.controllers import main
from api.oauth.token import oauth
from api.routers.controllers import routers
from api.blacklist.controllers import blacklist
from api.configurations.controllers import configurations

app = Flask(__name__)


@app.before_request
def option_autoreply():
    """ Always reply 200 on OPTIONS request """
    if request.method == 'OPTIONS':
        resp = app.make_default_options_response()

        headers = None
        if 'ACCESS_CONTROL_REQUEST_HEADERS' in request.headers:
            headers = request.headers['ACCESS_CONTROL_REQUEST_HEADERS']

        h = resp.headers
        # Allow the origin which made the XHR
        h['Access-Control-Allow-Origin'] = '*'
        # Allow the actual method
        h['Access-Control-Allow-Methods'] = request.headers['Access-Control-Request-Method']
        # Allow for 10 seconds
        h['Access-Control-Max-Age'] = "10"

        # We also keep current headers
        if headers is not None:
            h['Access-Control-Allow-Headers'] = headers

        return resp


@app.after_request
def after_request(response):
	if 'Access-Control-Allow-Origin' in response.headers:
		print 'origin exists'
	else:
		response.headers.add('Access-Control-Allow-Origin', '*')
	response.headers.add('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, accept, content-type')
	response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS,PATCH')
	return response

app.register_blueprint(main, url_prefix='/')
app.register_blueprint(oauth, url_prefix='/api')
app.register_blueprint(routers, url_prefix='/routers')
app.register_blueprint(blacklist, url_prefix='/blacklists')
app.register_blueprint(configurations, url_prefix='/configurations')
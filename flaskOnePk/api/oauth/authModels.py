from mongothon import Schema, create_model, Mixed, Array
from scripts import mongoHandler
from datetime import datetime

#need this because of the ObjectId that doesn't exists in mongothon
from bson.objectid import ObjectId
import schemer

class Schema(schemer.Schema):
    """A Schema encapsulates the structure and constraints of a Mongo document."""
  
    def __init__(self, doc_spec, **kwargs):
        super(Schema, self).__init__(doc_spec, **kwargs)
  
        # Every mongothon schema should expect an ID field.
        if '_id' not in self._doc_spec:
            self._doc_spec['_id'] = {"type": ObjectId}
#end

def make_models():
    db = mongoHandler.get()

    logsSchema = Schema({
        "timestamp" : {"type" : datetime, "default" : datetime.utcnow},
        "userHostAddress" : {"type" : str, "required" : True},
        "url" : {"type" : unicode, "required" : True},
        "requestVerb" : {"type" : str, "required" : True},
        "requestBody" : {"type" : str, "required" : True},
        #"headers" : {"type" : dict},
        #"requestValues" : {"type" : dict, "required" : True}
    })
    global Log
    Log = create_model(logsSchema, db['logs'])

    ipAttempts = Schema({
        "userHostAddress": {"type" : str, "required" : True},
        "timestamp": {"type" : datetime, "default" : datetime.utcnow},
        "attemptCount": {"type" : int, "required" : True},
        "isBlocked": {"type" : bool, "required" : True},
    })
    global ipAttempt
    ipAttempt = create_model(ipAttempts, db['ipAttempts'])

    applications = Schema({
        "name" : {"type" : str, "required" : True},
        "key" : {"type" : str, "required" : True},
        "secret": {"type" : str, "required" : True},
        "type" : { "type": str, "required" : True},
        "platform": {"type" : str, "required" : True},
        "uri": {"type" : str, "required" : True},
        "grant_types": {"type": list, "required" : True}
    })
    global application
    application = create_model(applications, db['applications'])

    accessTokens = Schema({
        "token": { "type": str, "required": True},
        "expires": {"type" : datetime, "default" : datetime.utcnow},
        "isValid": {"type" : bool, "required": True},
        "grantType": {"type" : str, "required" : True},
        "issuedAt": {"type" : datetime, "default" : datetime.utcnow},
        "scopes": { "type": list, "required": True},
        "deviceID": {"type" : str},
        "uuid": {"type" : str},
        "user": { "type": Mixed(basestring, int) },
        "applicationID": { "type": ObjectId, "required" : True},
    })
    global accessToken
    accessToken = create_model(accessTokens, db['accessTokens'])

    users = Schema({
        "username": { "type": str , "required" : True},
        "hash": { "type": str , "required" : True},
        "salt": { "type": str , "required" : True},
        "isBlocked": { "type" : bool },
        "attemptCount": { "type" : int },
        "role": { "type": str},
        "timestamp": { "type" : datetime },
        "createdAt": { "type" : datetime },
        "email": { "type": str , "required" : True}
        })
    global user
    user = create_model(users, db['users'])

    routers = Schema({
        "ip": { "type": basestring , "required" : True},
        "name": { "type": basestring , "required" : True},
        "statistics": { "type" : basestring },
        "description": { "type" : basestring },
        "interface": { "type" : basestring, "required" : True},
        "events": { "type" : basestring },
        "status": { "type" : basestring },
        })
    global router
    router = create_model(routers, db['routers'])
    #TODO: change schemas
    notifications = Schema({
        "ip": { "type": basestring , "required" : True},
        "name": { "type": basestring , "required" : True},
        "statistics": { "type" : basestring },
        "description": { "type" : basestring },
        "events": { "type" : basestring },
        "status": { "type" : basestring },
        })
    global notification
    notification = create_model(notifications, db['notifications'])

    states = Schema({
        "ip": { "type": basestring , "required" : True},
        "name": { "type": basestring , "required" : True},
        "statistics": { "type" : basestring },
        "description": { "type" : basestring },
        "events": { "type" : basestring },
        "status": { "type" : basestring },
        })
    global state
    state = create_model(states, db['states'])

    blacklists = Schema({
        "name": { "type": str , "required" : True},
        "level": { "type": int , "required" : True},
        "email": { "type" : int, "required" : True},
        })
    global blacklist
    blacklist = create_model(blacklists, db['blacklists'])

    configurations = Schema({
        "configArray": { "type": Array(unicode) , "required" : True},
        "main": {"type": bool, "required": True}
        })
    global configuration
    configuration = create_model(configurations, db['configurations'])

    notifications = Schema({
        "type": { "type": str , "required" : True},
        "ip": { "type": basestring , "required" : True},
        "datetime": {"type": datetime, "default": datetime.utcnow},
        "load": { "type": float },
        "reliability": { "type": float },
        "usage": { "type": float },
        })
    global notification
    notification = create_model(notifications, db['notifications'])

    print 'Finished creating schema models'

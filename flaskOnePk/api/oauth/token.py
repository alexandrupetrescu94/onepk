from flask import jsonify, Blueprint, request
from datetime import datetime, timedelta
from mongothon import ValidationException
from base64 import b64encode, b64decode
from os import urandom
from functools import wraps, update_wrapper

import authModels
from scripts import config

oauth = Blueprint('oauth', __name__)

def getUserHostAddress(request):
    if (request.headers.get('x-forwarded-for')):
        userHostAddress = request.headers.get('x-forwarded-for')
    else: 
        userHostAddress = request.environ.get('HTTP_X_REAL_IP', request.remote_addr) #if the user uses a proxy get the real address
    return userHostAddress

def maliciousAttempt(userHostAddress):
    userAttempt = authModels.ipAttempt.find_one({"userHostAddress": userHostAddress})
    userAttempt.timestamp = datetime.utcnow()
    if (userAttempt.attemptCount >= 10):
        userAttempt.isBlocked = True;
    else:
        userAttempt.attemptCount+=1
    userAttempt.save()

def log_attempt(userHostAddress, url, method, data):
    try:
        log = authModels.Log({
            "timestamp" : datetime.utcnow(),
            "userHostAddress" : userHostAddress,
            "url" : url,
            "requestVerb" : method,
            "requestBody" : data,
        })
        authModels.Log.validate(log)
    except ValidationException, e:
        return False
    log.save()
    return True

def ip_attempt(userHostAddress):
    userAttempt = authModels.ipAttempt.find_one({"userHostAddress": userHostAddress})
    if ( userAttempt == None ):
        try:
            ipAttempt = authModels.ipAttempt({
                "userHostAddress" : userHostAddress,
                "timestamp" : datetime.utcnow(),
                "attemptCount" : 0 ,
                "isBlocked" : False,
            })
            authModels.ipAttempt.validate(ipAttempt)
        except ValidationException, e:
            return False
    else:
        if ( userAttempt.isBlocked ):
            return False
    return True

def app_check(authHeader):
    if ( (authHeader != None) & (authHeader.lower().find('basic') != -1) ):
        #check if the appKey and appSecret are correct
        t = authHeader.split(' ')
        if ( (len(t) == 2) & (t[0].lower() == 'basic') ):
            clientBase64 = t[1].strip()
        buff = b64decode(clientBase64)
        clientString = buff.split(':')
        if ( len(clientString) == 2 ):
            clientID = clientString[0]
            clientSecret = clientString[1]
            application = authModels.application.find_one({ "key" : clientID, "secret" : clientSecret})
            if ( application == None ):
                return False
    return application['_id']

@oauth.route('/signup', methods=['POST'])
def signup():
    #Test with ip is_blocked
    #Test with normal sign up
    userHostAddress = getUserHostAddress(request)

    l = log_attempt(userHostAddress, request.url, request.method, request.data)
    if (not l):
        return jsonify({'status':500, 'message':'Error on login'})

    ip = ip_attempt(userHostAddress)
    if (not ip):
        return jsonify({'status':401, 'message':'Ip is blocked'})

    appObjId = app_check(request.headers.get('Authorization'))
    if(not appObjId):
        return jsonify({'status':500, 'message':'Application not available'})

    username = request.form.get('username', type=str)
    password = request.form.get('password', type=str) + config.config["salt"]
    email = request.form.get('email', type=str)
    try:
        #TODO VALIDATE USER
        user = authModels.user({
            "username": username,
            "hash": password,
            "salt": config.config["salt"],
            "isBlocked": False,
            "attemptCount": 0,
            "role": "user",
            "timestamp": datetime.utcnow(),
            "createdAt": datetime.utcnow(),
            "email": email
        })
        authModels.user.validate(user)
    except ValidationException, e:
        maliciousAttempt(userHostAddress)
        return jsonify({'status':500, 'message':'Input error'}) 
    user.save()
    return jsonify({'status':200, 'message': 'User created'})

@oauth.route('/login', methods=['POST'])
def login():
    #Test with ip is_blocked
    #Test with normal sign in
    userHostAddress = getUserHostAddress(request)

    l = log_attempt(userHostAddress, request.url, request.method, request.data)
    if (not l):
        return jsonify({'status':500, 'message':'Error on login'})

    ip = ip_attempt(userHostAddress)
    if (not ip):
        return jsonify({'status':401, 'message':'Ip is blocked'})

    appObjId = app_check(request.headers.get('Authorization'))
    if(not appObjId):
        return jsonify({'status':500, 'message':'Application not available'})
    
    username = request.form.get('username', type=str)
    password = request.form.get('password', type=str) + config.config["salt"]

    user = authModels.user.find_one({ "username" : username, "hash" : password})
    if ( user == None ):
        maliciousAttempt(userAttempt)
        return jsonify({'status':500, 'message':'No such user'})

    #generate token
    token_str = urandom(64).encode('hex')
    token_expires = datetime.now() + timedelta(minutes=60)
    try:
        token = authModels.accessToken({
            "token" : token_str,
            "expires" : token_expires,
            "isValid" : True,
            "grantType" : 'password', #request.form.get('grant_type'), #password
            "issuedAt" : datetime.utcnow(),
            "scopes" : ['app_scope'],
            "applicationID" : appObjId
        })
        authModels.accessToken.validate(token)
    except ValidationException, e:
        return jsonify({'status':500, 'message':'Error on login'})
    token.save()
    print 'asd'
    return jsonify({
        "access_token": token_str,
        "token_type": 'bearer',
        "expires_in": (token_expires - datetime.utcnow()).seconds,
        "scopes": token['scopes']
    })

def authorize(scopes):
    def decorator(f):
        def wrapped_function(*rejected):
            token = authModels.accessToken.find_one({'token':request.args.get('access_token')})
            if (token == None):
                rejected = jsonify({'status':500, 'message':'Please login'})
            else:
                if scopes:
                    if (token['scopes']):
                        if not token['scopes'][0] in scopes:
                            rejected = jsonify({'status':500, 'message':'Not Authorized'})
                    else:
                        rejected = jsonify({'status':500, 'message':'todo'})
                else:
                    #user token expired
                    if (token['expires']):
                        if (token['expires'] - datetime.utcnow()).seconds < 0 :
                            rejected = jsonify({'status':500, 'message':'Access Token error'})
                    else:
                        rejected = jsonify({'status':500, 'message':'todo'})
                
            return f(rejected)
        return update_wrapper(wrapped_function, f)
    return decorator

from flask import jsonify, Blueprint, Response, Request, request
from scripts import onepk
from scripts import redisHandler
from api.oauth import token
from api.oauth import authModels
from bson import ObjectId

blacklist = Blueprint('blacklist', __name__)

@blacklist.route('/', methods=['GET','OPTIONS'])
@token.authorize(scopes = ['app_scope'])
def index(rejected):
	if (rejected): #if error on token authorization
		return rejected
	cursor = authModels.blacklist.find();
	blacklists = list()
	for c in cursor:
		c['_id'] = str(c['_id'])
		blacklists.append(c)
	return jsonify({"blacklists":blacklists})

@blacklist.route('/<string:blacklist_id>/level', methods=['PUT','POST'])
def blacklist_level(blacklist_id):
	data = request.get_json()
	blacklist = authModels.blacklist.update({
	  '_id': ObjectId(blacklist_id)
	},{
	  '$set': {
	    'level': data.get('level')
	  }
	})
	return jsonify({'message':'ok','item': blacklist})

@blacklist.route('/<string:blacklist_id>/email', methods=['PUT','POST'])
def blacklist_email(blacklist_id):
	data = request.get_json()
	blacklist = authModels.blacklist.update({
	  '_id': ObjectId(blacklist_id)
	},{
	  '$set': {
	    'email': data.get('email')
	  }
	})
	return jsonify({'message':'ok','item': blacklist})

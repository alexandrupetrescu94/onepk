from api import app
from scripts import mongoHandler
from scripts import redisHandler
from api.oauth import authModels

if __name__ == '__main__':
	redisHandler.run()
	mongoHandler.run()
	authModels.make_models()
	app.run(debug=True)
